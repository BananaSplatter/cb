#ifndef CONVERT_H
#define CONVERT_H


#include <stdio.h>
#include <stdlib.h>
  int result;

int residu(int N)
{
    int i=0;
    while(N>=1){
        N=N/10;
        i++;
    }
    return i;
}


int power(int a, int b){
    int i;
    int result=1;
    for (i=0;i<b;i++){result=a*result;}
    return result;
}


int bin2dec(char* num, int length)
{
    int i;
    int result=0;
    int resto=0;
    int N = atoi(num);


    for(i=0;i<residu(N);i++){
        if(resto+ power(10,residu(N)-1-i) <= N)
        {
            resto = resto+power(10,residu(N)-1-i);
            result=power(2,residu(N)-1-i)+result;
        }
    }

  return result;
}

#endif
