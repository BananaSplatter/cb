# Algoritmo para Conversão de Base - Método Divisão Sucessivas

## Descrição
Programa em C, que faz a conversão de um número binário para um decimal.

## Como utilizar o CONVERSOR DE BASE
Para usar a biblioteca adicione o `convert.h` no seu código:

```c
#include <stdio.h>
#include "convert.h"
```

Para rodar exemplo, use o seguinte comando:

```bash
gcc ./src/main.c -I ./Include/ -o main
./main
```


## Autores
Esse algoritmo foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9502407/avatar.png?width=400)  | Navinchandry Ruas | [@BananaSplatter](https://gitlab.com/BananaSplatter) | [nruas@alunos.utfpr.edu.br](mailto:nruas@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/10256709/avatar.png?width=400)  | Matias Garcia Castilho | [@MatiasGarciaCastilho](https://gitlab.com/MatiasGarciaCastilho) | [matiascastilho@alunos.utfpr.edu.br](mailto:matiascastilho@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9975701/avatar.png?width=400)  | Hernandes Nascimento | [@nande6](https://gitlab.com/nande6) | [hernandesj@alunos.utfpr.edu.br](mailto:hernandesj@alunos.utfpr.edu.br)

